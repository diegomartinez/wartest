
---
-- Miscellaneous utilities.
--
-- @module wartest.utils

local stringex = require "stringex"

local utils = { }

---
-- TODO: Doc.
--
-- @tparam table super
-- @treturn table t
function utils.indexer(super)
	return setmetatable({ }, {
		__index = function(_, k)
			local v = super[k]
			if type(v) == "table" then
				return utils.indexer(v)
			end
			return v
		end,
	})
end

---
-- TODO: Doc.
--
-- @tparam string filename
-- @treturn table|nil|boolean conf
-- @treturn string error
function utils.loadconf(filename)
	local file, ok, err, data, func
	file, err = io.open(filename)
	if not file then
		return false, err
	end
	data, err = file:read("*a")
	file:close()
	if not data then
		return nil, err
	end
	func, err = loadstring(data, "@"..filename)
	if not func then
		return nil, err
	end
	local env = { }
	setfenv(func, env)
	ok, err = pcall(func)
	if not ok then
		return nil, err
	end
	return env
end

---
-- TODO: Doc.
--
-- @tparam khub.user filename
-- @treturn string s
function utils.usertostring(user)
	return ((user.name or "")
			..((user.host and user.port)
				and "@"..user.host..":"..user.port
				or "")
			..(user.id and "#"..user.id or ""))
end

local function fpprintf(file, prefix, fmt, ...)
	local str = fmt:format(...)
	for line in stringex.isplit(str, "\n") do
		file:write(prefix, line, "\n")
	end
end

---
-- TODO: Doc.
--
-- @tparam string fmt
-- @tparam any ...
function utils.eprintf(fmt, ...)
	return fpprintf(io.stderr, "", fmt, ...)
end

---
-- TODO: Doc.
--
-- @tparam string fmt
-- @tparam any ...
function utils.printf(fmt, ...)
	return fpprintf(io.stdout, os.date("%Y-%m-%d %H:%M:%S: "), fmt, ...)
end

---
-- TODO: Doc.
--
-- @tparam table|string val
-- @treturn wartest.rectangle
function utils.parsegeometry(val)
	if type(val) == "table" then
		return val
	end
	local w, h, x, y
	w, h, x, y = val:match(
			"^(%-?[0-9]+)x(%-?[0-9]+)"
			.."([+-][0-9]+)([+-][0-9]+)$")
	if tonumber(w) and tonumber(h)
			and tonumber(x) and tonumber(y) then
		return {
			w=tonumber(w), h=tonumber(h),
			x=tonumber(x), y=tonumber(y),
		}
	end
	w, h = val:match("^(%-?[0-9]+)x(%-?[0-9]+)$")
	if tonumber(w) and tonumber(h) then
		return { w=tonumber(w), h=tonumber(h) }
	end
	x, y = val:match("^([+-][0-9]+)([+-]([0-9]+)$")
	if tonumber(x) and tonumber(y) then
		return { x=tonumber(x), y=tonumber(y) }
	end
end

return utils
