
---
-- Communication module.
--
-- Submodules are exported as fields of this module.
--
-- @module wartest.comm
-- @see wartest.comm.Client
-- @see wartest.comm.Server

local comm = { }

comm.Client = require("wartest.comm.Client")
comm.Server = require("wartest.comm.Server")

return comm
