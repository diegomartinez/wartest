
---
-- User interface module.
--
-- Submodules are exported as fields of this module.
--
-- @module wartest.ui
-- @see wartest.ui.Form

local ui = { }

ui.Form = require "wartest.ui.Form"

return ui
