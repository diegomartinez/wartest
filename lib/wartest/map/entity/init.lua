
---
-- Entities.
--
-- Submodules are exported as fields of this module.
--
-- @module wartest.map.entity
-- @see wartest.map.entity.Entity
-- @see wartest.map.entity.Cowboy

local entity = { }

entity.Entity = require "wartest.map.entity.Entity"
entity.Cowboy = require "wartest.map.entity.Cowboy"

return entity
