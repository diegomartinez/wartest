
---
-- 2D vector math and helpers.
--
-- @module wartest.vmath

local v2math = { }

---
-- Add two vectors.
--
-- @tparam number x1 First vector X.
-- @tparam number y1 First vector Y.
-- @tparam number x2 Second vector X.
-- @tparam number y2 Second vector Y.
-- @treturn number New vector X.
-- @treturn number New vector Y.
function v2math.add(x1, y1, x2, y2)
	return x1 + x2, y1 + y2
end

---
-- Subtract a vector from another.
--
-- @tparam number x1 First vector X.
-- @tparam number y1 First vector Y.
-- @tparam number x2 Second vector X.
-- @tparam number y2 Second vector Y.
-- @treturn number New vector X.
-- @treturn number New vector Y.
function v2math.sub(x1, y1, x2, y2)
	return x1 - x2, y1 - y2
end

---
-- Multiply two vectors.
--
-- @tparam number x1 First vector X.
-- @tparam number y1 First vector Y.
-- @tparam number x2 Second vector X.
-- @tparam number y2 Second vector Y.
-- @treturn number New vector X.
-- @treturn number New vector Y.
function v2math.mul(x1, y1, x2, y2)
	return x1 * x2, y1 * y2
end

---
-- Divide a vector by another.
--
-- @tparam number x1 First vector X.
-- @tparam number y1 First vector Y.
-- @tparam number x2 Second vector X.
-- @tparam number y2 Second vector Y.
-- @treturn number New vector X.
-- @treturn number New vector Y.
function v2math.div(x1, y1, x2, y2)
	return x1 / x2, y1 / y2
end

---
-- Calculate the dot product of two vectors.
--
-- @tparam number x1 First vector X.
-- @tparam number y1 First vector Y.
-- @tparam number x2 Second vector X.
-- @tparam number y2 Second vector Y.
-- @treturn number Dot product.
function v2math.dot(x1, y1, x2, y2)
	return x1 * x2 + y1 * y2
end

---
-- Calculate the length of a vector.
--
-- @tparam number x X extent.
-- @tparam number y Y extent.
-- @treturn number Length.
function v2math.len(x, y)
	return math.sqrt(x * x + y * y)
end

---
-- Calculate distance between two points.
--
-- @tparam number x1 First point X.
-- @tparam number y1 First point Y.
-- @tparam number x2 Second point X.
-- @tparam number y2 Second point Y.
-- @treturn number Distance.
function v2math.dist(x1, y1, x2, y2)
	return v2math.len(x2 - x1, y2 - y1)
end

---
-- Normalize a vector to an unit vector.
--
-- @tparam number x X extent.
-- @tparam number y Y extent.
-- @treturn number New X extent.
-- @treturn number New Y extent.
function v2math.norm(x, y)
	local l = v2math.len(x, y)
	return x / l, y / l
end

---
-- Test if a point is inside an arc.
--
-- @tparam number x Point X.
-- @tparam number y Point Y.
-- @tparam number cx Arc center X.
-- @tparam number cy Arc center Y.
-- @tparam number d Arc center direction.
-- @tparam number r Arc radius.
-- @tparam number a Arc angle (centered on `d`).
-- @treturn boolean True if the point is inside, false otherwise.
function v2math.pointinarc(x, y, cx, cy, d, r, a)
	-- https://stackoverflow.com/a/1167047
	if v2math.dist(x, y, cx, cy) > r then
		return false
	end
	local vcpx, vcpy = v2math.norm(x - cx, y - cy)
	local vccx, vccy = v2math.norm(math.sin(d) * r, math.cos(d) * r)
	return math.acos(v2math.dot(vcpx, vcpy, vccx, vccy)) < a/2
end

---
-- Get the closest point on a segment from a point.
--
-- @tparam number x Point X.
-- @tparam number y Point Y.
-- @tparam number lx1 Segment start X.
-- @tparam number ly1 Segment start Y.
-- @tparam number lx2 Segment end X.
-- @tparam number ly2 Segment end Y.
-- @treturn number Closest X.
-- @treturn number Closest Y.
function v2math.closestpointonseg(x, y, lx1, ly1, lx2, ly2)
	local svx, svy = lx2 - lx1, ly2 - ly1
	-- XXX: WTF??
	local pvx, pvy = x - lx1, y - ly1
	local sux, suy = v2math.norm(svx, svy)
	local p = v2math.dot(sux, suy)
	if p <= 0 then
		return lx1, ly1
	elseif p >= v2math.len(svx, svy) then
		return lx2, ly2
	end
	pvx, pvy = sux * p, suy * p
	return pvx + lx1, pvy + ly1
end

---
-- Test if a circle intersects a line segment.
--
-- @tparam number cx Circle center X.
-- @tparam number cy Circle center Y.
-- @tparam number cr Circle radius.
-- @tparam number lx1 Segment start X.
-- @tparam number ly1 Segment start Y.
-- @tparam number lx2 Segment end X.
-- @tparam number ly2 Segment end Y.
-- @treturn number X offset to stop collision, or nil if not colliding.
-- @treturn number Y offset to stop collision, or nil if not colliding.
function v2math.isectcirseg(cx, cy, cr, lx1, ly1, lx2, ly2)
	local cpx, cpy = v2math.closestpointonseg(cx, cy, lx1, ly1, lx2, ly2)
	local dvx, dvy = cx - cpx, cy - cpy
	local dvl = v2math.len(dvx, dvy)
	if dvl > 0 and dvl <= cr then
		return dvx / dvl * (cr - dvl), dvy / dvl * (cr - dvl)
	end
end

return v2math
