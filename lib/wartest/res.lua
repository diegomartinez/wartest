
---
-- TODO: Doc.
--
-- @module wartest.res

local filesystem = love and love.filesystem
local graphics = love and love.graphics
local sound = love and love.sound

local res = { }

---
-- TODO: Doc.
--
-- @tfield string prefix
res.prefix = filesystem and filesystem.getWorkingDirectory() or ""

local function makegetter(loader)
	local cache = setmetatable({ }, { __mode="v" })
	return function(name, ...)
		local r = cache[name]
		if not r then
			r = loader(name, ...)
			cache[name] = r
		end
		return r
	end
end

---
-- TODO: Doc.
--
-- @function getimage
-- @tparam string filename
-- @treturn love.graphics.Image image
res.getimage = makegetter(function(filename)
	return assert(graphics and graphics.newImage)(filename)
end)

---
-- TODO: Doc.
--
-- @function getsound
-- @tparam string filename
-- @treturn love.sound.Source sound
res.getsound = makegetter(function(filename)
	return assert(sound and sound.newSource)(filename)
end)

---
-- TODO: Doc.
--
-- @function gettable
-- @tparam string filename
-- @treturn table t
res.gettable = makegetter(function(filename)
	local file = assert(io.open(res.prefix.."/"..filename, "rb"))
	local data = assert(file:read("*a"))
	file:close()
	local func = assert(loadstring(data))
	setfenv(func, { })
	local t = func()
	return assert(type(t) == "table" and t, "data is not a table")
end)

return res
