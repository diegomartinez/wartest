
---
-- TODO: Doc.
--
-- **Extends:** `klass`
--
-- @classmod wartest.map.entity.Entity

local graphics = love and love.graphics
local utils = require "wartest.utils"

local klass = require "klass"
local Entity = klass:extend("wartest.map.entity.Entity")

---
-- TODO: Doc.
--
-- @tfield number x
Entity.x = 0

---
-- TODO: Doc.
--
-- @tfield number y
Entity.y = 0

---
-- TODO: Doc.
--
-- @tfield number yaw
Entity.yaw = 0

---
-- TODO: Doc.
--
-- @tfield number hp
Entity.hp = 1

---
-- TODO: Doc.
--
-- @tfield number maxhp
Entity.maxhp = 1

---
-- Constructor.
--
-- @tparam table props
function Entity:init(props)
	props = props or { }
	for k, v in pairs(props) do
		if type(v) == "table" then
			self[k] = utils.indexer(v)
		else
			self[k] = v
		end
	end
end

---
-- TODO: Doc.
--
function Entity:draw()
	assert(graphics, "graphics not available")
	graphics.push()
	graphics.translate(self.x, self.y)
	self:paint()
	graphics.pop()
end

---
-- TODO: Doc.
--
function Entity:paint()
end

---
-- TODO: Doc.
--
function Entity:update()
end

return Entity
