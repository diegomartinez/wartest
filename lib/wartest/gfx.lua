
---
-- Graphics routines.
--
-- @module wartest.gfx

local graphics = love and love.graphics
local stringex = require "stringex"

local gfx = { }

local font

---
-- Initialize internal state.
--
function gfx.init()
	assert(graphics, "graphics not available")
	graphics.setDefaultFilter("nearest", "nearest")
	font = graphics.setNewFont("data/gfx/KPixelFont.ttf", 8)
	--font = graphics.setNewFont(8)
end

---
-- TODO: Doc.
--
-- @tparam string str
-- @tparam ?number x 0
-- @tparam ?number y 0
-- @tparam ?number halign 0
-- @tparam ?number valign 0
function gfx.print(str, x, y, halign, valign)
	assert(graphics, "graphics not available")
	x, y, halign, valign = x or 0, y or 0, halign or 0, valign or 0
	local tw, th, lh = gfx.textsize(str)
	local oldcolor = { graphics.getColor() }
	local oldfont = graphics.getFont()
	graphics.setFont(font)
	for line in stringex.isplit(str, "\n") do
		graphics.setColor(0, 0, 0, 128)
		graphics.print(line, x-tw*halign+1, y-th*valign+1)
		graphics.setColor(oldcolor)
		graphics.print(line, x-tw*halign, y-th*valign)
		y = y + lh
	end
	graphics.setFont(oldfont)
end

---
-- TODO: Doc.
--
-- @tparam string str
-- @treturn number width
-- @treturn number height
-- @treturn number lineheight
function gfx.textsize(str)
	assert(graphics, "graphics not available")
	local lineh = font:getHeight("Ay")
	local toth, maxw = 0, 0
	for line in stringex.isplit(str, "\n") do
		local linew = font:getWidth(line)
		maxw = math.max(maxw, linew)
		toth = toth + lineh
	end
	return maxw, toth, lineh
end

return gfx
