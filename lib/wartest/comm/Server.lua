
---
-- TODO: Doc.
--
-- **Extends:** `khub.server`
--
-- @classmod wartest.comm.Server

local socket = require "socket"
local v2math = require "wartest.v2math"
local weapons = require "wartest.data.weapons"
local Cowboy = require "wartest.map.entity.Cowboy"

local server = require "khub.server"
local Server = server:extend("wartest.comm.Server")

---
-- TODO: Doc.
--
Server.tickinterval = 1/20

---
-- TODO: Doc.
--
-- @tfield number lasttick
Server.lasttick = nil

Server.commands = setmetatable({ }, { __index=server.commands })
Server.appid = "me.kaeza.wartest"

---
-- TODO: Doc.
--
-- @tparam khub.user user
-- @tparam number x
-- @tparam number y
-- @tparam number yaw
function Server:onsetpos(user, x, y, yaw)
end

---
-- TODO: Doc.
--
-- @tparam khub.user user
-- @tparam table options
function Server:onsetoptions(user, options)
end

---
-- TODO: Doc.
--
-- @tparam khub.user user
-- @tparam table options
function Server:onshoot(user, weapon)
end

function Server:onjoin(user)
	user.entity = Cowboy({ id=user.id })
	user.posdirty = true
end

Server:registercommand("setpos", function(self, user, x, y, yaw)
	if type(x) ~= "number"
			or type(y) ~= "number"
			or type(yaw) ~= "number" then
		return self:onjunk(user, "setpos", x, y, yaw)
	end
	local e = user.entity
	if x ~= e.x or y ~= e.y or yaw ~= e.yaw then
		e.x, e.y, e.yaw = x, y, yaw
		user.posdirty = true
	end
	self:onsetpos(user, x, y, yaw)
end)

Server:registercommand("setoptions", function(self, user, options)
	if type(options) ~= "table" then
		return self:onjunk(user, "setoptions", options)
	end
	if type(options.color) ~= "table"
			or type(options.color[1]) ~= "number"
			or type(options.color[2]) ~= "number"
			or type(options.color[3]) ~= "number"
			or (options.color[4] ~= nil
				and type(options.color[4]) ~= "number") then
		return self:onjunk(user, "setoptions", options)
	end
	user.entity.color = options.color or user.entity.color
	self:broadcast(user, "setoptions", user.id, options)
	self:onsetoptions(user, options)
end)

Server:registercommand("shoot", function(self, user, weapon)
	if type(weapon) ~= "string" then
		return self:onjunk(user, "shoot", weapon)
	end
	local wdef = weapons[weapon]
	if not wdef then
		return
	end
	local range, spread = wdef.range, wdef.spread or 0
	local e = user.entity
	local dir = e.yaw + math.pi*spread*(math.random()-.5)
	local sx = e.x
	local sy = e.y
	local dx = sx + math.sin(dir)*range
	local dy = sx + math.cos(dir)*range
	self:onshoot(user, weapon, sx, sy, dx, dy, nil, nil)
	self:broadcast(nil, "shoot", user.id, weapon, sx, sy, dx, dy, nil, nil)
end)

local function sendsetpos(self, src)
	for dst in self:iterusers() do
		if src ~= dst then
			local srcent = src.entity
			local dstent = dst.entity
			if srcent and dstent and v2math.pointinarc(srcent.x, srcent.y,
					dstent.x, dstent.y,
					dstent.yaw+math.pi/2,
					120, math.pi) then
				self:sendto(dst, "setpos", src.id,
						srcent.x, srcent.y, srcent.yaw)
			end
		end
	end
end

local function tick(self)
	for u in self:iterusers() do
		if u.entity then
			u.entity:update()
			sendsetpos(self, u)
		end
	end
end

function Server:step()
	local now = socket.gettime()
	local diff = self.lasttick and (now - self.lasttick)
	local n = (diff and math.floor(diff/self.tickinterval) or 1)
	self.lasttick = now - (diff and diff%self.tickinterval or 0)
	for _ = 1, n do
		tick(self)
	end
	return server.step(self)
end

return Server
