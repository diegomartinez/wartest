
---
-- Wartest main module.
--
-- Submodules are exported as fields of this module.
--
-- @module wartest
-- @see wartest.gfx
-- @see wartest.intl
-- @see wartest.map
-- @see wartest.res
-- @see wartest.utils
-- @see wartest.v2math

local wartest = { }

wartest.gfx    = require "wartest.gfx"
wartest.intl   = require "wartest.intl"
wartest.map    = require "wartest.map"
wartest.res    = require "wartest.res"
wartest.utils  = require "wartest.utils"
wartest.v2math = require "wartest.v2math"

---
-- Table representing a rectangle.
--
-- @table rectangle
-- @tfield ?number x X position.
-- @tfield ?number y Y position.
-- @tfield ?number w Width.
-- @tfield ?number h Height.

return wartest
