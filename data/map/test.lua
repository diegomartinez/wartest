return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "2017.06.27",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 40,
  height = 40,
  tilewidth = 16,
  tileheight = 16,
  nextobjectid = 14,
  properties = {},
  tilesets = {},
  layers = {
    {
      type = "objectgroup",
      name = "walls",
      visible = true,
      opacity = 1,
      offsetx = -28,
      offsety = 12,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 1,
          name = "",
          type = "",
          shape = "rectangle",
          x = 91.0325,
          y = 326.371,
          width = 367,
          height = 17.2746,
          rotation = -41.345,
          visible = true,
          properties = {}
        },
        {
          id = 4,
          name = "",
          type = "",
          shape = "rectangle",
          x = 118.855,
          y = 324.783,
          width = 152.513,
          height = 17.9508,
          rotation = 44.9369,
          visible = true,
          properties = {}
        },
        {
          id = 5,
          name = "",
          type = "",
          shape = "rectangle",
          x = 380.863,
          y = 95.7861,
          width = 149.676,
          height = 17.9508,
          rotation = 44.9369,
          visible = true,
          properties = {}
        },
        {
          id = 6,
          name = "",
          type = "",
          shape = "rectangle",
          x = 212.033,
          y = 447.371,
          width = 144.489,
          height = 17.2746,
          rotation = -41.345,
          visible = true,
          properties = {}
        },
        {
          id = 7,
          name = "",
          type = "",
          shape = "rectangle",
          x = 363.461,
          y = 311.313,
          width = 162.566,
          height = 17.2746,
          rotation = -41.345,
          visible = true,
          properties = {}
        },
        {
          id = 12,
          name = "",
          type = "spawn",
          shape = "rectangle",
          x = 400,
          y = 402,
          width = 82,
          height = 86,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 13,
          name = "",
          type = "spawn",
          shape = "rectangle",
          x = 146.39,
          y = 319.418,
          width = 82,
          height = 86,
          rotation = 320.484,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
