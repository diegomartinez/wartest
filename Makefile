
.PHONY: default all love doc lint clean distclean

default: all

APPNAME = wartest
APPVER = scm-0

ZIP ?= zip -u
ZIPFLAGS ?= -9

LDOC ?= ldoc
LDOCFLAGS ?=

LUAMIN ?= luamin
LUAMINFLAGS ?=

LUACHECK ?= luacheck
LUACHECKFLAGS ?= --quiet --no-color

RM = rm -f
MKDIR = mkdir -p

all: love

MAINLOVE = $(APPNAME).love

MAINSRCLIB = \
	lib/klass.lua \
	lib/gettext.lua \
	lib/minser.lua \
	lib/stringex.lua \
	\
	lib/khub/init.lua \
	lib/khub/common.lua \
	lib/khub/client.lua \
	lib/khub/server.lua \
	\
	lib/simpleui/init.lua \
	lib/simpleui/Box.lua \
	lib/simpleui/Button.lua \
	lib/simpleui/Check.lua \
	lib/simpleui/Entry.lua \
	lib/simpleui/Image.lua \
	lib/simpleui/Label.lua \
	lib/simpleui/Option.lua \
	lib/simpleui/Slider.lua \
	lib/simpleui/Widget.lua \
	\
	lib/wartest/comm/init.lua \
	lib/wartest/comm/Client.lua \
	lib/wartest/comm/Server.lua \
	lib/wartest/map/init.lua \
	lib/wartest/map/entity/init.lua \
	lib/wartest/map/entity/Cowboy.lua \
	lib/wartest/map/entity/Entity.lua \
	lib/wartest/ui/Form.lua \
	lib/wartest/init.lua \
	lib/wartest/main.lua \
	lib/wartest/gfx.lua \
	lib/wartest/intl.lua \
	lib/wartest/res.lua \
	lib/wartest/utils.lua \
	lib/wartest/v2math.lua

MAINSRCMIN = $(patsubst lib/%.lua,lm/%.lm,$(MAINSRCLIB))

MAINSRCALL = $(MAINSRCMIN) main.lua

MAINDATA = \
	data/def/weapons.lua \
	\
	data/gfx/cowboy.png \
	data/gfx/KPixelFont.ttf \
	\
	data/sfx/weapons/revolver.wav \
	data/sfx/weapons/bigvolver.wav \
	data/sfx/weapons/sawnoff.wav \
	data/sfx/weapons/shotgun.wav \
	data/sfx/weapons/rifle.wav \
	\
	data/sfx/blip.wav \
	data/sfx/powerup.wav

love: $(MAINLOVE)

$(MAINLOVE): $(MAINSRCALL) $(MAINDATA)
	$(ZIP) $(ZIPFLAGS) $(MAINLOVE) $(MAINSRCALL) $(MAINDATA)

lm/%.lm: lib/%.lua
	@$(MKDIR) `dirname $@`
	$(LUAMIN) $(LUAMINFLAGS) -f $< > $@

docs:
	cd doc && $(LDOC) $(LDOCFLAGS) .

lint:
	$(LUACHECK) $(LUACHECKFLAGS) .

clean:
	$(RM) $(MAINSRCMIN)

distclean: clean
	$(RM) $(MAINLOVE)
