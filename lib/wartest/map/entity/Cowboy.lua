
---
-- TODO: Doc.
--
-- **Extends:** `wartest.map.entity.Entity`
--
-- @classmod wartest.map.entity.Cowboy

local graphics = love and love.graphics
local res = require "wartest.res"

local Entity = require "wartest.map.entity.Entity"
local Cowboy = Entity:extend("wartest.map.entity.Cowboy")

---
-- TODO: Doc.
--
Cowboy.weapon = "revolver"

---
-- TODO: Doc.
--
Cowboy.color = ({ 255, 255, 255 })

function Cowboy:init(props)
	self.image = graphics and res.getimage("data/gfx/cowboy.png")
	return Entity.init(self, props)
end

local frames = {
	revolver  = { 1, 0 },
	bigvolver = { 2, 0 },
	sawnoff   = { 3, 0 },
	shotgun   = { 1, 1 },
	rifle     = { 2, 1 },
}

function Cowboy:paint()
	if self.hp > 0 then
		local fx, fy = unpack(frames[self.weapon])
		local q1 = graphics.newQuad(
				0, fy*16, 16, 16, self.image:getDimensions())
		local q2 = graphics.newQuad(
				fx*16, fy*16, 16, 16, self.image:getDimensions())
		local function draw(x, y, r, g, b, a)
			graphics.push()
			graphics.translate(x, y)
			graphics.rotate(-self.yaw)
			graphics.setColor(r, g, b, a)
			graphics.draw(self.image, q1, -8, -8)
			graphics.setColor(255, 255, 255)
			graphics.draw(self.image, q2, -8, -8)
			graphics.pop()
		end
		draw(2, 2, 0, 0, 0, 64)
		draw(0, 0, unpack(self.color))
	else
		local q = graphics.newQuad(0, 32, 16, 16, self.image:getDimensions())
		local function draw(x, y, r, g, b, a)
			graphics.push()
			graphics.translate(x, y)
			graphics.rotate(-self.yaw)
			graphics.setColor(r, g, b, a)
			graphics.draw(self.image, q, -8, -8)
			graphics.pop()
		end
		draw(2, 2, 0, 0, 0, 64)
		draw(0, 0, unpack(self.color))
	end
end

return Cowboy
