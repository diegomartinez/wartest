
---
-- TODO: Doc.
--
-- @module wartest.intl

do
	local ok, lib = pcall(require, "gettext")
	if ok then
		return lib
	end
end

local intl = { }

---
-- TODO: Doc.
--
function intl.gettext(str)
	return str
end

---
-- TODO: Doc.
--
function intl.ngettext(sing, plur, n)
	return n == 1 and sing or plur
end

---
-- TODO: Doc.
--
function intl.bindtextdomain(d, dir)
end

---
-- TODO: Doc.
--
function intl.textdomain(d)
end

return intl
