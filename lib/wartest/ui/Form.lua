
---
-- Form widget.
--
-- @classmod wartest.ui.Form

local graphics = love.graphics

local Widget = require "simpleui.Widget"
local Form = Widget:extend("wartest.ui.Form")

function Form:paintbg()
	local oldfont = graphics.getFont()
	local font = self.font or oldfont
	local pl, pt, pr = self:paddings()
	local tbh = font:getHeight("Ay")
	graphics.setColor(self.bgcolor)
	graphics.rectangle("fill", 0, 0, self.w, self.h)
	graphics.setColor(0, 0, 128)
	graphics.rectangle("fill", pl, pt, self.w-pl-pr, tbh+4)
	graphics.setColor(self.bordercolor)
	graphics.rectangle("line", 0, 0, self.w, self.h)
	graphics.rectangle("line", pl+2, pt+2, self.w-pl-pr-4, tbh+4)
	graphics.setFont(font)
	graphics.print(self.text, pl+4, pt+4)
	graphics.setFont(oldfont)
end

function Form:calcminsize()
	local font = self.font or graphics.getFont()
	local pl, pt, pr, pb = self:paddings()
	local tbw, tbh = font:getWidth(self.text), font:getHeight("Ay")
	local minw, minh = tbw, 0
	local client = self[1]
	if client then
		local cw, ch = client:minsize()
		minw = math.max(minw, cw)
		minh = ch
	end
	return minw+pl+pr+4, minh+pt+pb+tbh+10
end

function Form:layout()
	local font = self.font or graphics.getFont()
	local pl, pt, pr, pb = self:paddings()
	local tbh = font:getHeight("Ay")
	local client = self[1]
	if client then
		client:reshape(pl+2, pt+tbh+8,
				self.w-pl-pr-4, self.h-pt-pb-tbh-10)
	end
end

return Form
