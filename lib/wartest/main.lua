
local graphics = love.graphics
local keyboard = love.keyboard
local mouse = love.mouse
local window = love.window
local socket = require "socket"
local argparse = require "argparse"
local stringex = require "stringex"
local simpleui = require "simpleui"
local gfx = require "wartest.gfx"
local res = require "wartest.res"
local utils = require "wartest.utils"
local Client = require "wartest.comm.Client"

local S = require "wartest.intl".gettext
local ME = love.filesystem.getIdentity()

local player, lastx, lasty, lastyaw
local root, view, chatbox

local conf
local userconf
local defconf = {
	name = os.getenv("LOGNAME") or "Player"..math.random(1, 99999),
	color = { 255, 255, 255 },
	host = "127.0.0.1",
	port = 12345,
	geometry = { w=800, h=600 },
}

local argparser = argparse(ME, "Wartest Client")

argparser:option("-a --address", "Connect to address",
		defconf.host, tostring, 1, "?"):target("host")
argparser:option("-p --port", "Connect to port",
		defconf.port, tonumber, 1, "?"):target("port")
argparser:option("-n --name", "Set player name",
		defconf.name, tostring, 1, "?"):target("map")
argparser:option("-g --geometry", "Set window geometry",
		defconf.geometry, utils.parsegeometry, 1, "?"):target("geometry")

local client = Client()

function client:onwelcome(name, desc)
	view:printf(S("*** Connected! Your ID is %d."), self.id)
	view:printf(S("*** Server name: %s"), name
			and name:gsub("\n", "\n*** ")
			or "---")
	view:printf(S("*** Server MOTD:\n*** %s"), desc
			and desc:gsub("\n", "\n*** ")
			or "---")
	Client.onwelcome(self, name, desc)
	player = self.entity
end

function client:onjoin(user)
	view:printf(S("* %s has joined."), utils.usertostring(user))
	return Client.onjoin(self, user)
end

function client:onquit(user)
	view:printf(S("* %s has quit."), utils.usertostring(user))
	return Client.onquit(self, user)
end

function client:onmessage(user, ispriv, message)
	view:printf(ispriv and ">%s< %s" or "<%s> %s",
			utils.usertostring(user), message)
	return Client.onmessage(self, user, ispriv, message)
end

function client:onjunk(...)
	view:printf(S("*** Junk data: %s"), stringex.concat(", ", ...))
end

function client:onshoot(user, weapon, sx, sy, dx, dy, hit, dmg)
	print("shoot:", user.id, weapon, sx, sy, dx, dy)
	view:addtrail(sx, sy, dx, dy)
	return Client.onshoot(self, user, sx, sy, dx, dy, hit, dmg)
end

local keys = { u="w", d="s", l="a", r="d" }
local function K(k)
	return keyboard.isDown(keys[k])
end

chatbox = simpleui.Entry {
	expand = true,
}

function chatbox:committed()
	if #self.text > 0 then
		client:sendmessage(self.text)
		view:printf("<%s> %s", client.name, self.text)
		self.text = ""
		self.index = 1
		view:setfocus()
	end
end

view = simpleui.Widget {
	canfocus = true,
	expand = true,
	_ticker = 0,
	_output = { },
	_trails = { },
}

function view:printf(fmt, ...)
	local str = fmt:format(...)
	for line in stringex.isplit(str, "\n") do
		table.insert(self._output, 1, {
			text = line,
			time = socket.gettime(),
		})
		self._output[6] = nil
	end
end

function view:addtrail(sx, sy, dx, dy)
	self._trails[#self._trails+1] = {
		time = 1,
		sx, sy,
		dx, dy,
	}
end

function view:textinput(text)
	if text == "t" then
		chatbox:setfocus()
		return true
	end
end

function view:update(dtime)
	local i, l = 1, #self._output
	local now = socket.gettime()
	while i <= l do
		local line = self._output[i]
		if now - line.time >= 5 then
			table.remove(self._output, i)
			l = l - 1
		else
			i = i + 1
		end
	end
	local ww, wh = window.getMode()
	local scale = math.max(ww, wh)/250
	for _, trail in ipairs(self._trails) do
		trail.time = trail.time - dtime
	end
	client:step(dtime)
	if player then
		local dx = self.focused and ((K"l" and -1) or (K"r" and 1)) or 0
		local dy = self.focused and ((K"u" and -1) or (K"d" and 1)) or 0
		player.x = player.x + dx*dtime*128
		player.y = player.y + dy*dtime*128
		local mx, my = mouse.getPosition()
		mx, my = (mx-self.x-self.w/2)/scale, (my-self.y-self.h/2)/scale
		player.yaw = math.atan2(mx, my)
		self._ticker = self._ticker + dtime
		if self._ticker >= .1 then
			self._ticker = 0
			local lx, ly = player.x, player.y
			local ld = player.yaw
			if lx ~= lastx or ly ~= lasty or ld ~= lastyaw then
				lastx, lasty, lastyaw = lx, ly, ld
				client:sendsetpos(lx, ly, ld)
			end
		end
	end -- if player
end

function view:mousepressed(_, _, b)
	if player then
		if b == self.LMB then
			client:sendshoot(player.weapon)
		end
	end
end

local function drawgameview(self)
	for _, trail in ipairs(self._trails) do
		graphics.setColor(255, 255, 255, 255*trail.time)
		graphics.line(unpack(trail))
	end
	graphics.setColor(255, 255, 255)
	for _, u in pairs(client.users) do
		local e = u.entity
		if e then
			e:draw()
		end
	end
	if player then
		player:draw()
	end
end

function view:paintbg()
	local scale = math.max(self.w, self.h)/250
	do graphics.push()
		graphics.translate(self.w/2, self.h/2)
		graphics.scale(scale)
		if player then
			graphics.translate(-player.x, -player.y)
		end
		local sc = { graphics.getScissor() }
		graphics.setScissor(0, 0, scale*300, scale*300)
		drawgameview(self)
		graphics.setScissor(unpack(sc))
	end graphics.pop()
	local mx, my = mouse.getPosition()
	mx, my = (mx-self.x-self.w)/scale/2, (my-self.y-self.h)/scale/2
	if player then
		gfx.print("ID: "..player.id
				.."\nPos: ("..player.x..","..player.y..")"
				.."\nYaw: "..player.yaw
				.."\nMouse: ("..mx..","..my..")"
				, 4, 4)
	end
	for i, line in ipairs(self._output) do
		gfx.print(line.text, 2, self.h-2, 0, i)
	end
end

root = simpleui.Box {
	mode = "v",
	view,
	simpleui.Box {
		mode = "h",
		chatbox,
		simpleui.Button {
			text = "Send",
			activated = function()
				return chatbox:committed()
			end,
		},
	},
} -- root

local function teardown()
	if client then
		client:shutdown()
		client = nil
	end
end

function love.quit()
	teardown()
end

local oldloveerrhand = love.errhand
function love.errhand(err)
	teardown()
	return oldloveerrhand(err)
end

function love.load(arg)
	local ok, err

	table.remove(arg, 1)
	ok, conf = argparser:pparse(arg)
	if not ok then
		utils.eprintf("%s: %s", ME, conf)
		return love.event.quit(1)
	end

	userconf = utils.loadconf("wartest.conf.lua") or { }

	setmetatable(userconf, { __index=defconf })
	setmetatable(conf, { __index=userconf })

	window.setMode(conf.geometry.w or 800,
			conf.geometry.h or 600,
			{ resizable = true })
	if conf.geometry.x or conf.geometry.y then
		local wx, wy = window.getPosition()
		window.setPosition(conf.geometry.x or wx, conf.geometry.y or wy)
	end
	window.setTitle("Wartest")
	window.setIcon(res.getimage("data/gfx/cowboy.png"):getData())
	gfx.init()
	graphics.setBackgroundColor(0, 192, 0)

	client.name = conf.name
	ok, err = client:connect(conf.host, conf.port)
	if not ok then
		utils.eprintf(S("%s: %s"), ME, err)
		return love.event.quit(1)
	end

	simpleui.run(root, 2)
	root:layout()
	view:setfocus()
end
