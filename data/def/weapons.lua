
local S = require "wartest.intl".gettext

return {
	revolver = {
		name   = S("Revolver"),
		power  =   15,
		delay  = 0.25,
		range  =  200,
		spread = 0.02,
		count  =    1,
		dual   = true,
	},
	bigvolver = {
		name   = S("Big revolver"),
		power  =   60,
		delay  = 1.00,
		range  =  300,
		spread = 0.01,
		count  =    1,
		dual   = true,
	},
	sawnoff = {
		name   = S("Sawn off shotgun"),
		power  =   10,
		delay  = 0.80,
		range  =  150,
		spread = 0.20,
		count  =    5,
		dual   = true,
	},
	shotgun = {
		name   = S("Shotgun"),
		power  =   15,
		delay  = 1.00,
		range  =  180,
		spread = 0.15,
		count  =    8,
	},
	rifle = {
		name   = S("Rifle"),
		power  =  120,
		delay  = 3.00,
		range  =  500,
		spread = 0.15,
		count  =    8,
	},
}
