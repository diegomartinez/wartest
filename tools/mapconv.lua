#! /usr/bin/env lua

local FULL = debug.getinfo(1).source:match("^@(.*)")
local ME = FULL:match("([^\\/]+)$")
local BASE = FULL:match("^(.*)[/\\]") or "."

package.path = (BASE.."/../lib/?.lua;"..BASE.."/../lib/?/init.lua;"
		..BASE.."/?.lua;"..BASE.."/?/init.lua;"
		..package.path)

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

local argparse = require "argparse"
local minser = require "minser"

local function loadtable(filename) -- luacheck: ignore
	local file = assert(io.open(filename))
	local data = assert(file:read("*a"))
	file:close()
	local func = assert(loadstring(data))
	setfenv(func, { })
	local t = func()
	return assert(type(t) == "table" and t, "data is not a table")
end

local function main(arg)
	local parser = argparse(ME,
			"Convert maps from an exported Tiled script.")
	parser:option("-o --output", "Set output file.")
	parser:argument("input", "Input file.",
			nil, nil, 1)
	arg = parser:parse(arg)
	print(minser.dump(arg))
end

return main(arg)
