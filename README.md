
# Wartest

## Requirements

### Common

* [argparse][argparse]
* [luasocket][luasocket]

### Client

* See *Common* above.
* [LÖVE][love2d] 0.10.2 or above.

### Server

* See *Common* above.

[argparse]: https://github.com/mpeterv/argparse
[luasocket]: https://github.com/diegonehab/luasocket
[love2d]: https://love2d.org
