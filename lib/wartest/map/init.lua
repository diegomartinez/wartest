
---
-- Map-related functions.
--
-- Submodules are exported as fields of this module.
--
-- @module wartest.map
-- @see wartest.map.entity

local map = { }

map.entity = require "wartest.map.entity"

return map
