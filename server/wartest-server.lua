#! /usr/bin/env lua

local FULL = debug.getinfo(1).source:match("^@(.*)")
local ME = FULL:match("([^\\/]+)$")
local BASE = FULL:match("^(.*)[/\\]") or "."

package.path = (BASE.."/../lib/?.lua;"..BASE.."/../lib/?/init.lua;"
		..package.path)

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

local argparse = require "argparse"
local stringex = require "stringex"
local res = require "wartest.res"
local utils = require "wartest.utils"
local Server = require "wartest.comm.Server"
local S = require "wartest.intl".gettext

do
	local HOME = os.getenv("HOME")
	if HOME and HOME ~= "" then
		res.prefix = HOME
	else
		res.prefix = BASE
	end
end

local server

local conf
local userconf
local defconf = {
	config = BASE.."/wartest-server.conf.lua",
	host = "0.0.0.0",
	port = 12345,
	map = "default",
	maxplayers = 16,
	name = "Wartest Server",
	motd = "Welcome to my server!",
}

local function parseargs(arg)
	local parser = argparse(ME, "Wartest Server")

	parser:option("-a --address", "Listen on address",
			defconf.host, tostring, 1, "?"):target("host")
	parser:option("-p --port", "Listen on port",
			defconf.port, tonumber, 1, "?"):target("port")
	parser:option("-m --map", "Set level",
			defconf.map, tostring, 1, "?"):target("map")
	parser:option("-P --max-players", "Set max players",
			defconf.maxplayers, tonumber, 1, "?"):target("maxplayers")
	parser:option("-n --name", "Set server name",
			defconf.name, tostring, 1, "?"):target("name")
	parser:option("-M --motd", "Set server Message Of The Day",
			defconf.motd, tostring, 1, "?"):target("motd")
	parser:option("-c --config", "Set configuration file",
			defconf.config, tostring, 1, "?"):target("config")

	return parser:pparse(arg)
end

local function teardown()
	if server then
		server:shutdown()
		server = nil
	end
end

local function main(arg)
	local ok, err

	ok, conf = parseargs(arg)
	if not ok then
		utils.eprintf("%s: %s", ME, conf)
		os.exit(1)
	end

	userconf, err = utils.loadconf(conf.config or defconf.config)
	if userconf == nil then
		utils.eprintf(S("%s: error loading configuration: %s"), ME, err)
		os.exit(1)
	elseif userconf == false then
		userconf = { }
	end

	setmetatable(userconf, { __index=defconf })
	setmetatable(conf, { __index=userconf })

	local srv = Server(conf.host, conf.port)
	srv.name = conf.name
	srv.description = conf.motd

	function srv:onjoin(user)
		utils.printf(S("* %s joined."),
				utils.usertostring(user))
		return Server.onjoin(self, user)
	end

	function srv:onquit(user, reason)
		if reason then
			utils.printf(S("* %s quit (%s)."),
					utils.usertostring(user),
					reason)
		else
			utils.printf(S("* %s quit."),
					utils.usertostring(user))
		end
		return Server.onquit(self, user, reason)
	end

	function srv:onmessage(user, target, message)
		if target then
			utils.printf(S("* [%s -> %s] %s"),
					utils.usertostring(user),
					utils.usertostring(target),
					message)
		else
			utils.printf(S("* <%s> %s"),
					utils.usertostring(user),
					message)
		end
		return Server.onmessage(self, user, target, message)
	end

	function srv:onjunk(user, ...)
		utils.printf(S("*** Junk data from %s: %s"),
				utils.usertostring(user),
				stringex.concat(", ", ...))
		return Server.onjunk(self, user, ...)
	end

	server = srv

	utils.printf(S("*** Server started. Press Ctrl+C to stop."))
	xpcall(function()
		srv:mainloop()
		teardown()
	end, function(errmsg)
		teardown()
		if not errmsg:match("interrupted!$") then
			local tb = debug.traceback()
			utils.printf(S("*** Server stopped due to an error: %s\n*** %s"),
					errmsg:gsub("\n", "\n*** "), tb:gsub("\n", "\n*** "))
			os.exit(1)
		end
	end)
	utils.printf(S("*** Server stopped successfully."))
end

return main(arg)
