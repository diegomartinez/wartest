
---
-- TODO: Doc.
--
-- **Extends:** `khub.client`
--
-- @classmod wartest.comm.Client

local socket = require "socket"
local Cowboy = require "wartest.map.entity.Cowboy"

local client = require "khub.client"
local Client = client:extend("wartest.comm.Client")

Client.appid = "me.kaeza.wartest"

---
-- TODO: Doc.
--
-- @tparam number x
-- @tparam number y
-- @tparam number yaw
-- @treturn number|nil sentok
-- @treturn ?string error
-- @treturn ?string rest
function Client:sendsetpos(x, y, yaw)
	return self:send("setpos", x, y, yaw)
end

---
-- TODO: Doc.
--
-- @tparam table options
-- @treturn number|nil sentok
-- @treturn ?string error
-- @treturn ?string rest
function Client:sendsetoptions(options)
	return self:send("setoptions", options)
end

---
-- TODO: Doc.
--
-- @tparam string weapon
-- @treturn number|nil sentok
-- @treturn ?string error
-- @treturn ?string rest
function Client:sendshoot(weapon)
	return self:send("shoot", weapon)
end

---
-- TODO: Doc.
--
-- @tparam khub.user user
-- @tparam number x
-- @tparam number y
-- @tparam number yaw
function Client:onsetpos(user, x, y, yaw)
end

---
-- TODO: Doc.
--
-- @tparam khub.user user
-- @tparam table options
function Client:onsetoptions(user, options)
end

---
-- TODO: Doc.
--
-- @tparam khub.user user
-- @tparam string weapon
-- @tparam number sx
-- @tparam number sy
-- @tparam number dx
-- @tparam number dy
-- @tparam wartest.hitinfo|nil hit
function Client:onshoot(user, weapon, sx, sy, dx, dy, hit, dmg)
end

function Client:onwelcome()
	self.entity = Cowboy({ id=self.id })
end

Client:registercommand("setpos", function(self, id, x, y, yaw)
	if type(id) ~= "number"
			or type(x) ~= "number"
			or type(y) ~= "number"
			or type(yaw) ~= "number" then
		return self:onjunk("setpos", id, x, y, yaw)
	end
	local user = id == self.id and self or self.users[id]
	if not user then
		return
	end
	user.lastseen = socket.gettime()
	if id == self.id or (not user.entity) then
		user.entity = user.entity or Cowboy({ id=user.id })
		user.entity.x = x
		user.entity.y = y
	else
		user.entity.targetx = x
		user.entity.targety = y
	end
	user.entity.yaw = yaw
	self:onsetpos(user, x, y, yaw)
end)

Client:registercommand("setoptions", function(self, id, options)
	if type(id) ~= "number"
			or type(options) ~= "table" then
		return self:onjunk("setoptions", id, options)
	elseif type(options.color) ~= "table"
			or type(options.color[1]) ~= "number"
			or type(options.color[2]) ~= "number"
			or type(options.color[3]) ~= "number"
			or (options.color[4] ~= nil
				and type(options.color[4]) ~= "number") then
		return self:onjunk("setoptions", id, options)
	end
	local user = self.users[id]
	if not user then
		return
	end
	user.options = options
	self:onsetoptions(user, options)
end)

Client:registercommand("shoot", function(self,
		id, weapon, sx, sy, dx, dy, hit, dmg)
	if type(id) ~= "number"
			or type(weapon) ~= "string"
			or type(sx) ~= "number"
			or type(sx) ~= "number"
			or type(dx) ~= "number"
			or type(dy) ~= "number"
			or (hit ~= nil and type(hit) ~= "number")
			or (dmg ~= nil and type(dmg) ~= "number")
			then
		return self:onjunk("shoot", id, weapon, sx, sy, dx, dy, hit, dmg)
	end
	local user = self.users[id]
	if not user then
		return
	end
	self:onshoot(user, weapon, sx, sy, dx, dy, hit, dmg)
end)

function Client:step(dtime)
	local now = socket.gettime()
	for _, u in pairs(self.users) do
		local e = u.entity
		if e then
			e:update(dtime)
			if e.targetx then
				local dx = (e.targetx - e.x)
				local dy = (e.targety - e.y)
				e.x = e.x + dx*dtime*10
				e.y = e.y + dy*dtime*10
			end
		end
		if now - (u.lastseen or 0) > 1 then
			u.entity = nil
		end
	end
	return client.step(self)
end

return Client
